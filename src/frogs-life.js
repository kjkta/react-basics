import * as React from "react";

const lifecycle = [
  {
    id: "constructor",
    name: "constructor()",
    phase: "Mounting",
    pos: { x: "5%", y: "80%" },
    img: "https://i.imgur.com/TN0rjwW.png",
    form: "tadpole"
  },
  {
    id: "componentWillMount",
    name: "componentWillMount()",
    phase: "Mounting",
    pos: { x: "7%", y: "60%" },
    img: "https://i.imgur.com/TN0rjwW.png",
    form: "tadpole"
  },
  {
    id: "render",
    name: "render()",
    phase: "Mounting",
    pos: { x: "20%", y: "37%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  },
  {
    id: "componentDidMount",
    name: "componentDidMount()",
    phase: "Mounting",
    pos: { x: "30%", y: "37%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  },
  {
    id: "componentWillReceiveProps",
    name: "componentWillReceiveProps()",
    phase: "Updating",
    pos: { x: "30%", y: "32%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  },
  {
    id: "shouldComponentUpdate",
    name: "shouldComponentUpdate()",
    phase: "Updating",
    pos: { x: "30%", y: "32%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  },
  {
    id: "componentWillUpdate",
    name: "componentWillUpdate()",
    phase: "Updating",
    pos: { x: "43%", y: "32%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  },
  {
    id: "render",
    name: "render()",
    phase: "Updating",
    pos: { x: "60%", y: "32%" },
    img: "https://i.imgur.com/vzhEO7t.png"
  },
  {
    id: "componentWillUnmount",
    name: "componentWillUnmount()",
    phase: "Unmounting",
    pos: { x: "85%", y: "65%" },
    img: "https://i.imgur.com/eJ4L48Y.png"
  }
];

export class ItsAFrogsLife extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: 0
    };
  }
  handleChangeStage({ keyCode }) {
    const back = keyCode === 219;
    const next = keyCode === 221;
    if (back || next) {
      const nextStage = this.state.stage + (back ? -1 : 1);
      if (lifecycle[nextStage]) {
        this.setState({ stage: nextStage });
      }
    }
  }
  renderFly() {
    return (
      <img
        style={{
          position: "absolute",
          top: 150,
          right: "40%",
          width: 40
        }}
        src="https://i.imgur.com/Y5kQIgI.png"
      />
    );
  }
  render() {
    const stage = lifecycle[this.state.stage];
    return (
      <Landscape
        stage={stage}
        tabIndex="0"
        onKeyDown={e => this.handleChangeStage(e)}
      >
        <img
          src={stage.img}
          style={{
            position: "absolute",
            width: 100,
            top: stage.pos.y,
            left: stage.pos.x,
            transition: "500ms all",
            ...(stage.form === "tadpole"
              ? {
                  transform: "rotate(-50deg)"
                }
              : {
                  transform: "rotate(-5deg)"
                }),
            ...(stage.phase === "Unmounting"
              ? {
                  transform: "rotate(180deg)"
                }
              : {})
          }}
        />
        {(stage.id === "componentWillReceiveProps" ||
          stage.id === "shouldComponentUpdate" ||
          stage.id === "componentWillUpdate") &&
          this.renderFly()}
      </Landscape>
    );
  }
}

const Landscape = ({ children, stage, ...props }) => (
  <div
    style={{
      height: "100vh",
      display: "flex",
      flexDirection: "column",
      fontFamily: "Open Sans",
      position: "relative",
      zIndex: 99
    }}
    {...props}
  >
    <div
      style={{
        height: "60vh",
        backgroundColor:
          stage.phase === "Updating"
            ? "#a9e1f5"
            : stage.phase === "Unmounting" ? "#1780a7" : "#e6f1f5",
        backgroundImage: "url(https://i.imgur.com/16yAqs9.png)",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "right bottom",
        backgroundSize: "80%",
        display: "flex",
        justifyContent: "center"
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <h2 style={{ margin: 10 }}>{stage.phase}</h2>
        <h1 style={{ margin: 0 }}>{stage.name}</h1>
      </div>
    </div>
    {children}
    <div
      style={{
        height: "40vh",
        backgroundColor: "#a3b8f7"
      }}
    />
  </div>
);

export default ItsAFrogsLife;