import * as React from "react";
import {
  Deck,
  Heading as SHeading,
  Image,
  ListItem,
  List,
  Slide,
  Text as SText,
  Notes,
  CodePane,
  Code as SCode,
  ComponentPlayground
} from "spectacle";
import createTheme from "spectacle/lib/themes/default";
import ItsAFrogsLife from "./frogs-life";
require("normalize.css");

const Heading = ({ children }) => (
  <SHeading style={{ padding: "25px 0", lineHeight: 1.2 }}>{children}</SHeading>
);
const Text = ({ children, style = {} }) => (
  <SText style={{ padding: "10px 0", ...style }}>{children}</SText>
);
const Arrow = ({ size, rotate }) => (
  <span
    style={{ fontSize: size || 60, transform: `rotate(${rotate || 0}deg)` }}
  >
    ➜
  </span>
);

const Code = ({ children }) => (
  <SCode style={{ padding: 15 }}>{children}</SCode>
);

const theme = createTheme(
  {
    primary: "white",
    secondary: "#333",
    tertiary: "#333",
    quartenary: "#333"
  },
  {
    primary: "Montserrat",
    secondary: "Helvetica"
  }
);

const styles = {
  heading: {
    padding: 20,
    fontSize: 42
  },
  codePane: {
    fontSize: "1.5vw",
    display: "inline-block",
    minWidth: 0,
    borderRadius: 3
  },
  list: {
    maxWidth: 800,
    margin: "30px auto"
  }
};

const Item = ({ children }) => (
  <ListItem style={{ margin: "20px 0", fontSize: 36 }}>{children}</ListItem>
);

export default class Presentation extends React.Component {
  render() {
    return (
      <React.Fragment>
        <style
          type="text/css"
          dangerouslySetInnerHTML={{
            __html: `
              .prism-code {
                padding: 15px !important;
              }
              .react-live:last-child :last-child {
                min-width: 65%;
              }
            `
          }}
        />
        <Deck
          transition={["slide"]}
          transitionDuration={500}
          theme={theme}
          contentWidth="90%"
          contentHeight="100%"
        >
          <Slide>
            <Heading>React JS ⚛️</Heading>
            <Text>For Beginners</Text>
            <Notes>Introduction, meet & greet</Notes>
          </Slide>
          <Slide>
            <Heading>Today's goals</Heading>
            <List style={styles.list}>
              <Item>Why use React</Item>
              <Item>Explore what React can do</Item>
              <Item>Build an interactive interface</Item>
            </List>
          </Slide>
          <Slide>
            <Heading>What is React?</Heading>
            <Notes>
              <p>Q: Who can tell me what React is</p>
              <p>
                React is an Open Source Javascript library created by Facebook
              </p>
            </Notes>
          </Slide>
          <Slide>
            <Text style={{ fontSize: 52, fontWeight: "bold", lineHeight: 1.6 }}>
              A <span style={{ color: "red" }}>JavaScript library</span> is a
              piece of <span style={{ color: "red" }}>pre-written</span>{" "}
              JavaScript which enables for{" "}
              <span style={{ color: "red" }}>easier development</span> of
              JavaScript-based applications.
            </Text>
          </Slide>
          <Slide>
            <Heading>React is a javascript toolbox</Heading>
            <span style={{ fontSize: 102, lineHeight: 2.4 }}>🔧 🗜 🔨</span>
            <Notes>
              <p>
                React has tools which enables us to write interactive
                applications faster
              </p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>Why use React?</Heading>
            <List style={styles.list}>
              <Item>State</Item>
              <Item>Component-Based</Item>
              <Item>Composability </Item>
            </List>
            <Notes>
              <p>How do we know React the right toolbox to use?</p>
            </Notes>
          </Slide>
          <Slide>
            <Heading style={styles.heading}>Where is React used?</Heading>
            <a
              href="https://trello.com/b/TPNx0kjT/kyles-board"
              target="_blank"
              rel="noopener noreferrer"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around"
              }}
            >
              <Text style={{ padding: 20 }}>Trello</Text>
              <Image src={require("./img/trello.png")} alt="trello-example" />
            </a>
            <Text style={{ fontSize: 52, padding: 70 }}>...?</Text>
            <Notes>
              <p>Q: What are other examples?</p>
              <p>AirBnb, Instagram, Facebook, Dropbox</p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>What does it do?</Heading>
            <List style={styles.list}>
              <Item>Renders HTML</Item>
              <Item>Updates HTML</Item>
              <Item>Manages state</Item>
            </List>
            <Notes>
              <p>
                Using "shadow DOM", keys which identify nodes to be updated.
              </p>
              <p>Makes it easy to access previous states and next states.</p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>Getting started</Heading>
            <Text>(using codesandbox.io)</Text>
            <Notes>
              <p>
                On codesandbox.io, wipe the index.js file and go through the
                required imports.
              </p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>Everything in React starts with a component</Heading>
          </Slide>
          <Slide>
            <Heading>A React component is just like a regular function</Heading>
            <Code>{`React.createElement(type, props, children)`}</Code>
            <List style={{ margin: 60 }}>
              <Item>
                type: {`<string>`} A HTML tag ("div", "span", "input", ...)
              </Item>
              <Item>
                props: {`<object>`} defines the properties (arguments)
              </Item>
              <Item>
                children: {`<string|React.Element>`} the contents of the element
              </Item>
            </List>
            <Notes>
              <p>Write a function using the React API</p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>Render your component to the DOM</Heading>
            <Code>{`ReactDOM.render(element, container)`}</Code>
            <List style={{ margin: 60 }}>
              <Item>
                element: {`<React.Element>`} the React component you want to
                render
              </Item>
              <Item>
                container: {`<DOMElement>`} where to insert the React element
                (ie. <Code>document.getElementById("root")</Code>)
              </Item>
            </List>
          </Slide>
          <Slide>
            <Heading>JSX (HTML in JS)</Heading>
            <Text>A "web" developers' best friend</Text>
            <div style={{ margin: 50 }}>
              <Code>{`React.createElement("div", {}, "Hello World")`}</Code>
              <span style={{ fontSize: 52, display: "block", padding: 30 }}>
                &#8681;
              </span>
              <Code>{`<div>Hello World</div>`}</Code>
            </div>
          </Slide>
          <Slide>
            <Heading>Customising a component</Heading>
            <div style={{ margin: 50 }}>
              <Code>new Person("Kyle", "Thomson")</Code>
              <span style={{ fontSize: 52, display: "block", padding: 30 }}>
                &#8681;
              </span>
              <Code>{`<Person name="Kyle" />`}</Code>
            </div>
          </Slide>
          <Slide>
            <Heading>State-ful components</Heading>
            <div style={{ margin: 50 }}>
              <Code>{`(props) => <div>Hello {props.name}</div>`}</Code>
              <span style={{ fontSize: 52, display: "block", padding: 30 }}>
                &#8681;
              </span>
              <CodePane
                style={styles.codePane}
                lang="javascript"
                source={`class EditPerson extends React.Component {
  state = {}
  render() {
    return <div>Hello {this.props.name}</div>
  }
}`}
              />
            </div>
            <Notes>
              <CodePane
                source={`class EditPerson extends React.Component {
  state = {
    name: this.props.name
  };
  render() {
    return (
      <div>
        <label htmlFor="name">
          First Name:
          <input
            id="name"
            value={this.state.name}
            onChange={e => this.setState({ name: e.target.value })}
          />
        </label>
        <div>Hello {this.state.name}</div>
      </div>
    );
  }
}`}
              />
            </Notes>
          </Slide>
          <Slide>
            <Heading>Add initial state</Heading>
            <Notes>
              <CodePane
                style={styles.codePane}
                lang="javascript"
                source={`state = {
  name: this.props.name
};`}
              />
            </Notes>
          </Slide>
          <Slide>
            <Heading>Update the state</Heading>
            <Notes>
              <CodePane
                style={{
                  ...styles.codePane,
                  minWidth: "100%",
                  display: "block"
                }}
                lang="javascript"
                source={`<label htmlFor="name">
  First Name:
  <input
    id="name"
    value={this.state.name}
    onChange={e => this.setState({ name: e.target.value })}
  />
</label>`}
              />
            </Notes>
          </Slide>
          <Slide>
            <Heading>Render feedback to our user</Heading>
            <Notes>
              <CodePane
                style={styles.codePane}
                lang="javascript"
                source={`<div>
  Hello{" "}
  {this.state.name.length > 0
    ? this.state.name
    : "<enter name>"}
</div>`}
              />
            </Notes>
          </Slide>
          <Slide>
            <Heading>Components are re-useable</Heading>
            <Text>(like regular functions)</Text>
            <Notes>
              <CodePane
                style={styles.codePane}
                lang="javascript"
                source={`const App = () => [
  <EditPerson name="Kyle" />,
  <EditPerson name="Bec" />
];
`}
              />
            </Notes>
          </Slide>
          <Slide>
            <Heading>What does this tell us?</Heading>
            <Text style={{ margin: 50, textAlign: "left" }}>
              React.Component is a toolbox which provides:
            </Text>
            <List>
              <Item>State management</Item>
              <Item>
                <Code>setState()</Code> method for updating state
              </Item>
              <Item>
                Props for customising components and initializing state
              </Item>
              <Item>Event handling</Item>
              <Item>
                Components for rendering different instances of the same logic
              </Item>
              <Item>and there's a little more...</Item>
            </List>
          </Slide>
          <Slide>
            <Heading>Let's build a todo list!</Heading>
            <Text>(using codesandbox.io)</Text>
            <Text>Create a React Component that renders "My todos"</Text>
            <Notes>
              <p>
                We will build a Component with some simple functions. You will
                have the choice and freedom to build te features you want!
              </p>
              <p>
                We will use codesandbox to fast-track sourcing our dependancies
              </p>
            </Notes>
          </Slide>
          <Slide>
            <ComponentPlayground
              code={`class TodoList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <div>
        <h1>My Todos</h1>
      </div>
    )
  }
}

render(<TodoList />)
          `}
            />
          </Slide>
          <Slide>
            <Heading>Add some props</Heading>
            <Text>What are the customisable properties?</Text>
            <List style={styles.list}>
              <Item>
                <Code>{`<TodoList listName="Shopping" />`}</Code>
              </Item>
              <Item>
                <Code>{`<h1>My {this.props.listName} List</h1>`}</Code>
              </Item>
            </List>
          </Slide>
          <Slide>
            <Heading>Initialise some state</Heading>
            <Text>What are the dynamic values?</Text>
            <List style={styles.list}>
              <Item>
                <Code>
                  {`this.state = {
                  todos: []
                }`}
                </Code>
              </Item>
              <Item>
                <Code>{`<p>{this.state.todos.join(", ")}</p>`}</Code>
              </Item>
            </List>
            <Notes>Q: What will ".join()" do?</Notes>
          </Slide>
          <Slide>
            <Heading>Conditionally render</Heading>
            <Text>What if we have no todos?</Text>
            <CodePane
              style={{ ...styles.codePane, minWidth: "100%" }}
              lang="javascript"
              source={`<p>
  {
    this.state.todos.length > 0 
      ? this.state.todos.join(", ")
        : "No " + this.props.listName + " items"
  }
</p>`}
            />
          </Slide>
          <Slide>
            <Heading>Add an initial todo</Heading>
            <Text>Let's check our rendering works</Text>
            <CodePane
              style={{ ...styles.codePane, minWidth: "100%" }}
              lang="javascript"
              source={`
// constructor method
this.state = {
  todos: props.todos || []
}

// render method
return (
  <TodoList 
    listName="Shopping"
    todos={[
      "My first todo"
    ]} 
  />
)`}
            />
            <Notes>
              Q: What happens if we do not specify a "todos" prop? Will our
              application break?
            </Notes>
          </Slide>
          <Slide>
            <Heading>Add a todo</Heading>
            <Text>Update our state</Text>
            <CodePane
              style={{ ...styles.codePane, minWidth: "100%" }}
              lang="javascript"
              source={`<input
  type="text"
  value={this.state.todoInput}            
  onChange={(e) => {
    this.setState({ todoInput: e.target.value })
  }}
/>
...
<button
  onClick={() => {
    if(this.state.todoInput.trim().length > 0) {
      const newTodos = this.state.todos.concat(this.state.todoInput)
      this.setState({ todos: newTodos, todoInput: "" })
    }
  }}
>
  Add todo
</button>`}
            />
          </Slide>
          <Slide>
            <Heading>Complete a todo</Heading>
            <Text>When a todo is clicked, remove it</Text>
            <CodePane
              style={{ ...styles.codePane, minWidth: "100%" }}
              lang="javascript"
              source={`{this.state.todos.map((todo, i) => (
  <span 
    onClick={() => {
      const newTodos = this.state.todos.filter(
        (todo, index) => index !== i
      )
      this.setState({ todos: newTodos })
    }}>
    {todo},{" "}
  </span>
))}`}
            />
            <Notes>
              <p>We will have to mature from our ".join()" method</p>
            </Notes>
          </Slide>
          <Slide>
            <Heading>What's your next feature?</Heading>
            <List>
              <Item>Completed todos list</Item>
              <Item>Sort todos</Item>
              <Item>Edit todo</Item>
              <Item>Press enter to add</Item>
              <Item>Saving to an external service</Item>
              <Item>...?</Item>
            </List>
          </Slide>
          <Slide>
            <Text>Slides: https://kjkta.gitlab.io/react-basics/</Text>
            <ComponentPlayground
              code={`class TodoList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      todos: props.todos || [],
      todoInput: ""
    }
  }
  render() {
    return (
      <div>
        <h1>My Todos</h1>
        <input
        type="text"
        value={this.state.todoInput}            
        onChange={(e) => {
          this.setState({ todoInput: e.target.value })
        }}
      />
      <button
        onClick={() => {
          if(this.state.todoInput.trim().length > 0) {
            const newTodos = this.state.todos.concat(this.state.todoInput)
            this.setState({ todos: newTodos, todoInput: "" })
          }
        }}
      >
        Add todo
      </button>
        <p>{this.state.todos.length > 0 
          ? this.state.todos.map((todo, i, todos) => (
            <span 
              onClick={() => {
                const newTodos = this.state.todos.filter(
                  (todo, index) => index !== i
                )
                this.setState({ todos: newTodos })
              }}>
              {todo}{(todos.length - 1) !== i ? ", " : "."}
            </span>
          ))
            : "No " + this.props.listName + " items"}
        </p>
      </div>
    )
  }
}

render(
  <TodoList 
    listName="Shopping"
    todos={[
      "My first todo"
    ]}  
  />
)
`}
            />
          </Slide>
          <Slide>
            <Heading>Thank you!</Heading>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%",
                maxWidth: 720,
                margin: "0 auto"
              }}
            >
              <Image
                src={require("./img/kyle.jpg")}
                style={{
                  borderRadius: "50%",
                  boxShadow: "2px 2px 5px 5px #ccc",
                  width: 200
                }}
              />
              <div style={{ textAlign: "left" }}>
                <Text>Kyle Thomson</Text>
                <Text>kjay.thomson@gmail.com</Text>
                <Text>
                  <a href="kylet.xyz">kylet.xyz</a>
                </Text>
                <Text>
                  <a href="https://www.linkedin.com/in/kyle-thomson/">
                    LinkedIn
                  </a>
                </Text>
              </div>
            </div>
          </Slide>
        </Deck>
      </React.Fragment>
    );
  }
}

const componentLifecycleSlides = (
  <React.Fragment>
    <Slide>
      <Heading style={styles.heading}>
        React is a living thing, it has a lifecycle
      </Heading>
    </Slide>
    <Slide>
      <ItsAFrogsLife />
      <Notes>
        <p>Use [ + ] to navigate back and forth</p>
        <p>Under water is prior to rendering</p>
        <p>
          You don't have to use ANY of these, but they are very useful for
          timing
        </p>
        <ul>
          <li>componentWillMount - Mostly used for server-side rendering</li>
          <li>componentDidMount - Fetch some data or subscribe to a service</li>
          <li>ComponentUpdate - Do we need to re-render given new props?</li>
          <li>componentWillUpdate - Cue transitions or animations</li>
          <li>componentDidUpdate - Send data to an external service</li>
          <li>
            componentWillUnmount - "Garbage collection" - Invalidating timers,
            stopping animations, closing network connections{" "}
          </li>
        </ul>
      </Notes>
    </Slide>
    <Slide>
      <Heading style={styles.heading}>What does mounting look like?</Heading>
      <CodePane
        style={{ ...styles.codePane, minWidth: "100%" }}
        lang="javascript"
        source={`class App extends React.Component {
constructor(props) {
super(props)
this.state = {
// Initialise state
}
}
componentWillMount() {
// Mostly used for server-side rendering
}
componentDidMount() {
// Fetch some data or subscribe to a service
}
...
render() { return <div />; }
}`}
      />
    </Slide>
    <Slide>
      <Heading style={styles.heading}>What does updating look like?</Heading>
      <CodePane
        style={{ ...styles.codePane, minWidth: "100%" }}
        lang="javascript"
        source={`class App extends React.Component {
shouldComponentUpdate(nextProps, nextState) {
// Do we need to re-render given new props?
}
componentWillUpdate(nextProps, nextState) {
// Cue transitions or animations
}
componentDidUpdate(prevProps, prevState) {
// Send data to an external service
}
...
render() {
return <div />;
}
}`}
      />
    </Slide>
    <Slide>
      <Heading style={styles.heading}>What does unmounting look like?</Heading>
      <CodePane
        style={{ ...styles.codePane, minWidth: "100%" }}
        lang="javascript"
        source={`class App extends React.Component {
...
componentWillUnmount() {
/* 
"Garbage collection"
Invalidating timers, stopping animations, 
closing network connections
*/
}
}`}
      />
    </Slide>
    <Slide>
      <Image
        src={require("./img/react-lifecycle-diagram.png")}
        alt="react-lifecycle"
      />
      <Notes>
        <p>Provide an example when recapping the flow.</p>
      </Notes>
    </Slide>
  </React.Fragment>
);
