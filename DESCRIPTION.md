**Details**

In this workshop, we will be covering the basics of React and how you can use it. You will build a "Reactive" todo application to and complete your tasks! You can tailor your application with features of your choice to best suit your purpose.

🎉 Our objectives: 🎉

→ Understand React and why we use it
→ Understand React's Component-based philosophy and Component lifecycle
→ Build a component and render HTML in the browser
→ Add interactive elements that accept user input
→ Update user interface with component state
→ Build a "Reactive" application to manage your todos
→ Add features which suit you best! This is a Workshop for Complete Beginners to jump start your relationship with React and it's possibilities (and yours).

🚀 This workshop will enable you to: 🚀

→ Get a solid beginners foundation of React
→ Understand the process of building React Components
→ Create interactive interfaces which "react" instantly to cues like user input

Above all this is a practical session, and we are here to learn together, make mistakes, ask questions and collaborate so that we can learn intuitively. My passion is to facilitate your learning and hopefully be a good teacher to help you assimilate concepts in 2 hours that typically take you days. Responsibility of learning lies with the teacher!

Le Wagon is a Coding school for creative entrepreneurial mindsets. Our only requirement from you is that you are eager to learn.

Pre-requisites:

→ Basic HTML knowledge
→ Basic JS knowledge

💻 What to bring & preparation (Make sure this to-do list is completed) 💻

[✔] Your laptop in your bag and charged

[✔] Google Chrome Downloaded (https://www.google.com/chrome/), installed and running on your computer 

----------------------

Look forward to meeting you soon!